package com.progetting.shared;

import static org.junit.Assert.*;

import org.junit.Test;

public class UtenteTest {

	@Test
	public void testGetUsername() {
		Utente utente = new Utente("utente","pass","email","nome","cognome","sesso","data","luogoNascita","domicilio",true);
		assertEquals("result","utente",utente.getUsername());
	}

	@Test
	public void testSetUsername() {
		Utente utente = new Utente("utente","pass","email","nome","cognome","sesso","data","luogoNascita","domicilio",true);
		utente.setUsername("userCambiato");
		assertEquals("result","userCambiato",utente.getUsername());
	}

	@Test
	public void testGetPassword() {
		Utente utente = new Utente("utente","pass","email","nome","cognome","sesso","data","luogoNascita","domicilio",true);
		assertEquals("result","pass",utente.getPassword());
	}

	@Test
	public void testSetPassword() {
		Utente utente = new Utente("utente","pass","email","nome","cognome","sesso","data","luogoNascita","domicilio",true);
		utente.setPassword("passCambiata");
		assertEquals("result","passCambiata",utente.getPassword());
	}

	@Test
	public void testGetEmail() {
		Utente utente = new Utente("utente","pass","email","nome","cognome","sesso","data","luogoNascita","domicilio",true);
		assertEquals("result","email",utente.getEmail());
	}

	@Test
	public void testGetIsGiudice() {
		Utente utente = new Utente("utente","pass","email","nome","cognome","sesso","data","luogoNascita","domicilio",true);
		assertEquals("result",true,utente.getIsGiudice());
	}

	@Test
	public void testGetIsGiudiceToString() {
		Utente utente = new Utente("utente","pass","email","nome","cognome","sesso","data","luogoNascita","domicilio",true);
		
		assertEquals("result","true",utente.getIsGiudiceToString());
	}

	@Test
	public void testSetIsGiudice() {
		Utente utente = new Utente("utente","pass","email","nome","cognome","sesso","data","luogoNascita","domicilio",true);
		utente.setIsGiudice(false);
		assertEquals("result",false,utente.getIsGiudice());
	}

}
