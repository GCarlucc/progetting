package com.progetting.shared;

import static org.junit.Assert.*;

import org.junit.Test;

public class GiudizioTest {

	@Test
	public void testGetId() {
		Giudizio giud = new Giudizio(1,5,"giudice",10);
		assertEquals("result",1,giud.getId());
	}

	@Test
	public void testGetVoto() {
		Giudizio giud = new Giudizio(1,5,"giudice",10);
		Integer voto = 5 ;
		assertEquals("result", voto, giud.getVoto());
	}

	@Test
	public void testGetGiudice() {
		Giudizio giud = new Giudizio(1,5,"giudice",10);
		assertEquals("result","giudice",giud.getGiudice());
	}

	@Test
	public void testGetIdRisposta() {
		Giudizio giud = new Giudizio(1,5,"giudice",10);
		assertEquals("result",10,giud.getIdRisposta());
	}

}
