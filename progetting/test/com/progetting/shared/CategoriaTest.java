package com.progetting.shared;

import static org.junit.Assert.*;

import org.junit.Test;

public class CategoriaTest {

	@Test
	public void testGetId() {
		Categoria cat = new Categoria(1, "Sport");
		
		assertEquals("result", 1, cat.getId());
		
	}

	@Test
	public void testGetNome() {
	    
		Categoria cat = new Categoria(1, "Sport");
		assertEquals("result", "Sport", cat.getNome());
		
	}

}
