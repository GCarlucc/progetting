package com.progetting.shared;

import static org.junit.Assert.*;

import org.junit.Test;

public class RispostaTest {

	@Test
	public void testGetId() {
		Risposta risp = new Risposta(1,"testo","autore","data",10);
		assertEquals("result",1,risp.getId());
	}

	@Test
	public void testGetText() {
		Risposta risp = new Risposta(1,"testo","autore","data",10);
		assertEquals("result","testo",risp.getText());
	}

	@Test
	public void testGetData() {
		Risposta risp = new Risposta(1,"testo","autore","data",10);
		assertEquals("result","data",risp.getData());
	}

	@Test
	public void testGetIdDomanda() {
		Risposta risp = new Risposta(1,"testo","autore","data",10);
		assertEquals("result",10,risp.getIdDomanda());
	}

	@Test
	public void testGetAutore() {
		Risposta risp = new Risposta(1,"testo","autore","data",10);
		assertEquals("result","autore",risp.getAutore());
	}

	@Test
	public void testGetMediaVoti() {
		Risposta risp = new Risposta(1,"testo","autore","data",10);
		Float mediaVoti = (float) 5;
		risp.setMediaVoti(mediaVoti);
		assertEquals("result",mediaVoti,risp.getMediaVoti());
	}

	@Test
	public void testSetMediaVoti() {
		Risposta risp = new Risposta(1,"testo","autore","data",10);
		Float mediaVoti = (float) 5;
		risp.setMediaVoti(mediaVoti);
		assertEquals("result",mediaVoti,risp.getMediaVoti());
	}

}
