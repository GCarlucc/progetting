package com.progetting.shared;

import static org.junit.Assert.*;

import org.junit.Test;

public class DomandaTest {

	@Test
	public void testGetId() {
		Domanda dom = new Domanda(1,"Testo ? ","Autore","data","root");
		assertEquals("result",1,dom.getId());
	}

	@Test
	public void testGetText() {
		Domanda dom = new Domanda(1,"Testo","Autore","data","root");
		assertEquals("result","Testo",dom.getText());
	}

	@Test
	public void testGetData() {
		Domanda dom = new Domanda(1,"Testo","Autore","data","root");
		assertEquals("result","data",dom.getData());
	}

	@Test
	public void testGetRoot() {
		Domanda dom = new Domanda(1,"Testo","Autore","data","root");
		assertEquals("result","root",dom.getRoot());
	}

	@Test
	public void testGetAutore() {
		Domanda dom = new Domanda(1,"Testo","Autore","data","root");
		assertEquals("result","Autore",dom.getAutore());
	}

	@Test
	public void testSetRoot() {
		
		Domanda dom = new Domanda(1,"Testo","Autore","data","root");
		dom.setRoot("rootDue");
		assertEquals("result","rootDue",dom.getRoot());
	}

}
