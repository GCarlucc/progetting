package com.progetting.server;

import com.progetting.client.GreetingService;
import com.progetting.client.Registrazione;
import com.progetting.shared.Categoria;
import com.progetting.shared.Domanda;
import com.progetting.shared.FieldVerifier;
import com.progetting.shared.Giudizio;
import com.progetting.shared.Risposta;
import com.progetting.shared.Utente;

import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Set;
import java.util.concurrent.ConcurrentNavigableMap;

import org.mapdb.DB;
import org.mapdb.DBMaker;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;

/**
 * The server-side implementation of the RPC service.
 */
@SuppressWarnings("serial")
public class GreetingServiceImpl extends RemoteServiceServlet implements GreetingService {
	
	
	///////////////////////////// LOGIN ////////////////////////////////////
	@Override
	public synchronized Utente effettuaLogin(String username, String password) throws IllegalArgumentException {
		File dbFile = new File("progettingdb");
        DB db = DBMaker.newFileDB(dbFile)
                .closeOnJvmShutdown()
                .make();
		ConcurrentNavigableMap<String, Utente> utenti = db.getTreeMap("utenti");
		Utente utente = utenti.get(username);
	    db.commit();
		db.close();
		if(utente.getPassword().equals(password)){
			return utente;
		}else{
			return null;
		}	
	}
	
    ///////////////////////////// UTENTI //////////////////////////////////////
	@Override
	public synchronized ArrayList<Utente> restituisciUtenti() throws IllegalArgumentException {
		
		File dbFile = new File("progettingdb");
        DB db = DBMaker.newFileDB(dbFile)
                .closeOnJvmShutdown()
                .make();
		
        ConcurrentNavigableMap<String, Utente> utenti = db.getTreeMap("utenti");
        
        ArrayList<Utente> listaUtenti = new ArrayList<Utente>();		
		Set<String> keys = utenti.keySet();
		for(String key : keys)
		{
			listaUtenti.add(utenti.get(key));
		}
		
		db.close();
		return listaUtenti;
			
	}
	
	
	///////////////////////////// GIUDICE ////////////////////////////////////
	
	/**
	 * Metodo per creare l'id del giudizio che verrà inserito.
	 * 
	 * @return l'id effetivamente assegnato
	 */
	@Override
	public synchronized int creazioneIdGiudizio() throws IllegalArgumentException{

		File dbFile = new File("progettingdb");
		DB db = DBMaker.newFileDB(dbFile)
				.closeOnJvmShutdown()
				.make();
	
		ConcurrentNavigableMap<Integer,Giudizio> giudizi = db.getTreeMap("giudizi");
		int dimensione;
		if (giudizi.lastEntry() == null){
			dimensione = 1;
		}
		else{
			dimensione = giudizi.lastKey() + 1;
		}
		db.commit();
		db.close();
		return dimensione;
	}

	/**
	 * Metodo per inserire un nuovo Giudice.
	 * 
	 * @param nuovoGiudizio - oggetto giudizio passato per essere inserito nel db
	 */
	@Override
	public synchronized void inserimentoGiudizio(Giudizio nuovoGiudizio) throws IllegalArgumentException{

		File dbFile = new File("progettingdb");
		DB db = DBMaker.newFileDB(dbFile)
				.closeOnJvmShutdown()
				.make();
	
		ConcurrentNavigableMap<Integer, Giudizio> giudizi = db.getTreeMap("giudizi");
		giudizi.put(nuovoGiudizio.getId(), nuovoGiudizio);
		db.commit();
		db.close();
	}

	/**
	 * Metodo per nominare un nuovo Giudice.
	 * 
	 * @param username - L'username dell'utente che si vuole rendere giudice
	 */
	@Override
	public synchronized void nominaGiudice(String username) throws IllegalArgumentException {

	File dbFile = new File("progettingdb");
	DB db = DBMaker.newFileDB(dbFile)
	.closeOnJvmShutdown()
	.make();
	
	ConcurrentNavigableMap<String, Utente> utenti = db.getTreeMap("utenti");
	
	Set<String> user = utenti.keySet();
	for(String key: user)
	{
	  if(utenti.get(key).getUsername().equalsIgnoreCase(username))
	    {
	        if(utenti.get(key).getIsGiudice() == true)
	          {
	        	    utenti.get(key).setIsGiudice(false);
				Utente cambioUtente = utenti.get(key);
				utenti.put(cambioUtente.getUsername(), cambioUtente);  
	          }else{
	        	    utenti.get(key).setIsGiudice(true);
			    Utente cambioUtente = utenti.get(key);
			    utenti.put(cambioUtente.getUsername(), cambioUtente);
	          }
	       db.commit();
        }
	}
	
	db.close();
	
	}

	/**
	 * Metodo per cercare una risposta già giudicata da quell'autore.
	 * 
	 * @param  idRisposta - l'id della risposta da controllare se già giudicata
	 * @param  autoreGiudizio - l'autore dell'eventuale giudizio
	 * 
	 * @return giudicata - true se è già stata giudicata 
	 */
	@Override
	public synchronized boolean seGiudicata(String autoreGiudizio, int idRisposta) throws IllegalArgumentException {
		boolean giudicata = false;
		File dbFile = new File("progettingdb");
		DB db = DBMaker.newFileDB(dbFile)
				.closeOnJvmShutdown()
				.make();
	
		ConcurrentNavigableMap<Integer, Giudizio> giudizi = db.getTreeMap("giudizi");
		Set<Integer> judgments = giudizi.keySet();
		for(Integer key: judgments){
			if(giudizi.get(key).getIdRisposta() == idRisposta && giudizi.get(key).getGiudice().equals(autoreGiudizio)){
				giudicata = true;
				break;
			}
		}
		return giudicata;
	}
	
	/**
	 * Metodo per cercare i giudici di una risposta.
	 * 
	 * @param  idRisposta - l'id della risposta che è associata ai giudici da cercare 
	 * 
	 * @return  listagiudici - arraylist di stringhe contenenti i giudici che hanno votato quella risposta  
	 */
	@Override
	public synchronized ArrayList<String> cercaGiudici(int idRisposta) throws IllegalArgumentException {
		ArrayList<String> listagiudici = new ArrayList<String>();
		File dbFile = new File("progettingdb");
        DB db = DBMaker.newFileDB(dbFile)
                .closeOnJvmShutdown()
                .make();
        
        ConcurrentNavigableMap<Integer, Giudizio> giudizi = db.getTreeMap("giudizi");
        Set<Integer> judgments = giudizi.keySet();
        for(Integer key: judgments){
        	if(giudizi.get(key).getIdRisposta() == idRisposta){
        		listagiudici.add(giudizi.get(key).getGiudice());
        	}
        }
        return listagiudici;
	}
	
	 ////////////////////////////////////// ADMIN ///////////////////////////////////////////
	 
		@Override
		public synchronized void inserimentoAdmin() throws IllegalArgumentException {
			
			File dbFile = new File("progettingdb");
	        DB db = DBMaker.newFileDB(dbFile)
	                .closeOnJvmShutdown()
	                .make();
	        
			ConcurrentNavigableMap<String, Utente> utenti = db.getTreeMap("utenti");
			
			if(!utenti.containsKey("admin"))
			{
				Utente admin = new Utente("admin","admin",null, null, null, null, null, null, null, true);
				utenti.put("admin", admin);
				
				db.commit();
				
			}
			db.close();	
		}

	 
	////////////////////////////////  REGISTRAZIONE ////////////////////////////////
	
	/* controllo che non ci siano utenti duplicati */
	@Override
	public synchronized boolean controllaRegUtente(String username) throws IllegalArgumentException {
		// TODO Auto-generated method stub
	
		boolean ritorno = false;
		Utente utente = null;
		
		File dbFile = new File("progettingdb");
        DB db = DBMaker.newFileDB(dbFile)
                .closeOnJvmShutdown()
                .make();
        
		ConcurrentNavigableMap<String, Utente> utenti = db.getTreeMap("utenti");
		Set<String> user = utenti.keySet();
		for(String key : user){
			 if (utenti.get(key).getUsername().equalsIgnoreCase(username)) {
				 ritorno = true;
				 break;
			 }
		}
		db.close();
		return ritorno; 
	}	
	
    /* effettuo la registrazione */

	@Override
	public synchronized void registrazioneUtente(Utente nuovoUtente) throws IllegalArgumentException {
		// TODO Auto-generated method stub
		
		
		String username = nuovoUtente.getUsername();
			
			File dbFile = new File("progettingdb");
	        DB db = DBMaker.newFileDB(dbFile)
	                .closeOnJvmShutdown()
	                .make();
	        
			ConcurrentNavigableMap<String, Utente> utenti = db.getTreeMap("utenti");
			utenti.put(username, nuovoUtente);
			db.commit();
			db.close();
		

	}
////////////////////////////////DOMANDE///////////////////////////////////

	@Override
	public synchronized void inserimentoDomanda (Domanda nuovaDomanda) throws IllegalArgumentException {
		// TODO Auto-generated method stub
			
			File dbFile = new File("progettingdb");
	        DB db = DBMaker.newFileDB(dbFile)
	                .closeOnJvmShutdown()
	                .make();
	        
		   ConcurrentNavigableMap<Integer, Domanda> domande = db.getTreeMap("domande");
		   domande.put(nuovaDomanda.getId(), nuovaDomanda);
		   db.commit();
		   db.close();
		

	}
	
	
	@Override
	public synchronized int creazioneIdDomanda() throws IllegalArgumentException{
		
		File dbFile = new File("progettingdb");
        DB db = DBMaker.newFileDB(dbFile)
                .closeOnJvmShutdown()
                .make();
        
        ConcurrentNavigableMap<Integer,Domanda> domande = db.getTreeMap("domande");
        int dimensione;
        if (domande.lastEntry() == null){
        	dimensione = 1;
        }else{
        dimensione = domande.lastKey() + 1;
        }
        db.commit();
		db.close();
        return dimensione;
        //
        
	}
	
	@Override
	public synchronized ArrayList<Domanda> visualizzaDomanda() throws IllegalArgumentException{
		
		File dbFile = new File("progettingdb");
        DB db = DBMaker.newFileDB(dbFile)
                .closeOnJvmShutdown()
                .make();
        
        ConcurrentNavigableMap<Integer,Domanda> domande = db.getTreeMap("domande");
        
        ArrayList<Domanda> listaDomande = new ArrayList<Domanda>();		
		Set<Integer> keys = domande.keySet();
		for(Integer key : keys){
			listaDomande.add(domande.get(key));
			}
		db.close();
		return listaDomande;
        
	}
	
	@Override
	public synchronized void eliminaDomanda (int id) throws IllegalArgumentException {
		// TODO Auto-generated method stub
			
			File dbFile = new File("progettingdb");
	        DB db = DBMaker.newFileDB(dbFile)
	                .closeOnJvmShutdown()
	                .make();
	        
		   ConcurrentNavigableMap<Integer, Domanda> domande = db.getTreeMap("domande");
		   domande.remove(id);
		   
		   ConcurrentNavigableMap<Integer, Risposta> risposte = db.getTreeMap("risposte");
		   ConcurrentNavigableMap<Integer, Giudizio> giudizi = db.getTreeMap("giudizi");
		
		   Set<Integer> keys = risposte.keySet();
			for(Integer key : keys)
			{
				if(risposte.get(key).getIdDomanda() == id)
				{
				
				   Set<Integer> chiavi = giudizi.keySet();
					for(Integer chiave : chiavi)
					{
						if(giudizi.get(chiave).getIdRisposta() == risposte.get(key).getId())
						{
							giudizi.remove(chiave);
						}
					}
					
			      risposte.remove(key);
			   }
		    }	
		   db.commit();
		   db.close();
		

	}
////////////////////////////////RISPOSTE ////////////////////////////////
	
	@Override
	public synchronized void inserimentoRisposta (Risposta nuovaRisposta) throws IllegalArgumentException {
		// TODO Auto-generated method stub
			
			File dbFile = new File("progettingdb");
	        DB db = DBMaker.newFileDB(dbFile)
	                .closeOnJvmShutdown()
	                .make();
	        
		   ConcurrentNavigableMap<Integer, Risposta> risposte = db.getTreeMap("risposte");
		   risposte.put(nuovaRisposta.getId(), nuovaRisposta);
		   db.commit();
		   db.close();
		

	}
	
	@Override
	public synchronized ArrayList<Risposta> visualizzaRisposta() throws IllegalArgumentException{
		
		File dbFile = new File("progettingdb");
        DB db = DBMaker.newFileDB(dbFile)
                .closeOnJvmShutdown()
                .make();
        
        ConcurrentNavigableMap<Integer,Risposta> risposte = db.getTreeMap("risposte");
        ConcurrentNavigableMap<Integer, Giudizio> giudizi = db.getTreeMap("giudizi");
        int idRisp;
        float voto = 0;
        float numgiudizi= 0;
        float media = 0;
        
        ArrayList<Risposta> listaRisposte = new ArrayList<Risposta>();		
		Set<Integer> keys = risposte.keySet();
		Set<Integer> chiavi = giudizi.keySet();
		for(Integer key : keys)
		{
			idRisp = risposte.get(key).getId();
			if(chiavi.isEmpty()) 
			{
				media = -1; 	
			}
			else
			{
				for(Integer chiave: chiavi)
				{
					if(giudizi.get(chiave).getIdRisposta() == idRisp)
					{
						voto = voto+giudizi.get(chiave).getVoto();
		        		    numgiudizi++;
					}
				}
				if(numgiudizi == 0)
				{
		        		media = -1;
		        }
		        else 
		        {
		        		media = (voto/numgiudizi);
		        }
			}
			
			risposte.get(key).setMediaVoti(media);
			Risposta cambioRisp = risposte.get(key);
			risposte.put(cambioRisp.getId(), cambioRisp);
			//risposte.put(risposte.get(key).getId(), risposte.get(key));
			listaRisposte.add(risposte.get(key));
			voto = 0;
			numgiudizi=0;
	} //fine for
	  db.commit();
	  db.close();
	  return listaRisposte;
        
	}
	
	@Override
	public synchronized int creazioneIdRisposta() throws IllegalArgumentException{
		
		File dbFile = new File("progettingdb");
		DB db = DBMaker.newFileDB(dbFile)
				.closeOnJvmShutdown()
				.make();
		
		ConcurrentNavigableMap<Integer, Risposta> risposte = db.getTreeMap("risposte");
		int dimensione;
		if(risposte.lastEntry() == null){
			dimensione = 1;
		} else{
			dimensione = risposte.lastKey() + 1;
		}
		db.commit();
		db.close();
		return dimensione;
	}
	
	@Override
	public synchronized void eliminaRisposta (int idRisposta) throws IllegalArgumentException {
		// TODO Auto-generated method stub
			
			File dbFile = new File("progettingdb");
	        DB db = DBMaker.newFileDB(dbFile)
	                .closeOnJvmShutdown()
	                .make();
	        
		   ConcurrentNavigableMap<Integer, Risposta> risposte = db.getTreeMap("risposte");
		   ConcurrentNavigableMap<Integer, Giudizio> giudizi = db.getTreeMap("giudizi");
			
		   Set<Integer> keys = giudizi.keySet();
			for(Integer key : keys)
			{
			  if(giudizi.get(key).getIdRisposta() == idRisposta)
			  {
				 giudizi.remove(key); 
			  }	
			}
			risposte.remove(idRisposta);
		    db.commit();
		    db.close();
		

	}
	
	
////////////////////////////////CATEGORIE ////////////////////////////////
		
	// POPOLO DELLE CATEGORIE INIZIALI
	
	@Override
	public synchronized void popolocategorie() throws IllegalArgumentException{
	
		File dbFile = new File("progettingdb");
        DB db = DBMaker.newFileDB(dbFile)
                .closeOnJvmShutdown()
                .make();
        
        ConcurrentNavigableMap<Integer,Categoria> categorie = db.getTreeMap("categorie");
        
        if(categorie.isEmpty())
        {
        		Categoria categoria = new Categoria(1,"Ambiente");
        		Categoria categoria2 = new Categoria(2,"Animali");
        		Categoria categoria3 = new Categoria(3,"Arte e Cultura");
        		Categoria categoria4 = new Categoria(4,"Elettronica e Tecnologia");
        		Categoria categoria5 = new Categoria(5,"Sport");
        		Categoria categoria6 = new Categoria(6,"Svago");
        		Categoria categoria7 = new Categoria(7,"Cucina");
        		categorie.put(categoria.getId(), categoria);
        		categorie.put(categoria2.getId(), categoria2);
        		categorie.put(categoria3.getId(), categoria3);
        		categorie.put(categoria4.getId(), categoria4);
        		categorie.put(categoria5.getId(), categoria5);
        		categorie.put(categoria6.getId(), categoria6);
        		categorie.put(categoria7.getId(), categoria7);
        		db.commit();
        		
	   }
        db.close();
	} 
	
	@Override
	public synchronized boolean controlloCategorieTopDuplicate(String inserita) throws IllegalArgumentException{
		boolean giaIns = false;
		File dbFile = new File("progettingdb");
        DB db = DBMaker.newFileDB(dbFile)
                .closeOnJvmShutdown()
                .make();
        ConcurrentNavigableMap<Integer,Categoria> categorie = db.getTreeMap("categorie");
        Set<Integer> keys = categorie.keySet();
        
        if(inserita.contains("/")){
        	String [] inseritaSplit = inserita.split("/");
        	for(Integer key : keys) {
        		if(categorie.get(key).getNome().contains("/")){
        			String newCat = inseritaSplit[inseritaSplit.length-2]+"/"+inseritaSplit[inseritaSplit.length-1];
        			if(categorie.get(key).getNome().contains(newCat)){
        				giaIns = true;
        				break;
        			}
        		}
        		else {
        			if(categorie.get(key).getNome().equalsIgnoreCase(inserita)){
        				giaIns = true;
        				break;
        			}
        		}
        	}
        }
        else {
        	for(Integer key : keys){
        		if(categorie.get(key).getNome().equalsIgnoreCase(inserita)){
        			giaIns = true;
    				break;
        		}
        	}
        }
        
        return giaIns;
	}
	
	@Override
	public synchronized ArrayList<String> visualizzaCategoria() throws IllegalArgumentException{

		
		File dbFile = new File("progettingdb");
        DB db = DBMaker.newFileDB(dbFile)
                .closeOnJvmShutdown()
                .make();
        
        ConcurrentNavigableMap<Integer,Categoria> categorie = db.getTreeMap("categorie");
        
		
        ArrayList<String> listaCategorie = new ArrayList<String>();		
		Set<Integer> keys = categorie.keySet();
		for(Integer key : keys){
			listaCategorie.add(categorie.get(key).getNome());
			}
		db.close();
		return listaCategorie;
	
	}
	
	@Override
	public synchronized int creazioneIdCategoria() throws IllegalArgumentException{
		
		File dbFile = new File("progettingdb");
		DB db = DBMaker.newFileDB(dbFile)
				.closeOnJvmShutdown()
				.make();
		
		ConcurrentNavigableMap<Integer, Categoria> categorie = db.getTreeMap("categorie");
		
		int dimensione = categorie.lastKey() + 1;
		
		db.close();
		return dimensione;
	}
	
	@Override
	public synchronized void inserimentoCategoria (Categoria nuovaCategoria) throws IllegalArgumentException {
		// TODO Auto-generated method stub
			
			File dbFile = new File("progettingdb");
	        DB db = DBMaker.newFileDB(dbFile)
	                .closeOnJvmShutdown()
	                .make();
	        
		   ConcurrentNavigableMap<Integer, Categoria> categorie = db.getTreeMap("categorie");
		   categorie.put(nuovaCategoria.getId(), nuovaCategoria);
		   db.commit();
		   db.close();
	}
	
	@Override
	public synchronized void rinominaCategoria (String nomeVecchio ,String nomeNuovo) throws IllegalArgumentException {
		// TODO Auto-generated method stub
			
			File dbFile = new File("progettingdb");
	        DB db = DBMaker.newFileDB(dbFile)
	                .closeOnJvmShutdown()
	                .make();
	        
		   ConcurrentNavigableMap<Integer, Categoria> categorie = db.getTreeMap("categorie");
		   ConcurrentNavigableMap<Integer, Domanda> domande = db.getTreeMap("domande");
		   
		   
		   Set<Integer> keys = categorie.keySet();
			for(Integer key : keys)
			{
				if(categorie.get(key).getNome().contains(nomeVecchio))
				{
					
					//cambio root domanda 
					Set<Integer> chiavi = domande.keySet();
					for(Integer chiave : chiavi)
					{
						if(domande.get(chiave).getRoot().contains(nomeVecchio))
						{
							String rootDomanda = domande.get(chiave).getRoot().replace(nomeVecchio, nomeNuovo);
							domande.get(chiave).setRoot(rootDomanda);
							Domanda rinominataDomanda = domande.get(chiave);
							domande.put(rinominataDomanda.getId(), rinominataDomanda);
							
						}
					}
					//cambio cat 
					//cicoc
					int nuovoId = categorie.get(key).getId(); 
					String n = "";
					if(nomeVecchio.contains("/")){
						String[] spl = nomeVecchio.split("/");
						n = categorie.get(key).getNome().replace(spl[1], nomeNuovo);
					}
					else{
						n = categorie.get(key).getNome().replace(nomeVecchio, nomeNuovo);
					}
					
					Categoria categoriaRinominata = new Categoria(nuovoId,n);
					categorie.put(nuovoId, categoriaRinominata);
					db.commit();
				}
			}
		   
		   
		   db.close();
	}


}
