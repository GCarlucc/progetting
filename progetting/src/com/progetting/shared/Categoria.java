package com.progetting.shared;

import java.io.Serializable;

public class Categoria implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	protected int id;
	protected String nome;
	
	public Categoria(){}
	
	public Categoria( int id, String nome ){
		this.id = id;
		this.nome = nome;
	}
	
	//GET 
	public int getId(){
	  return this.id;
	}
	
	public String getNome(){
		return nome;
	}

}
