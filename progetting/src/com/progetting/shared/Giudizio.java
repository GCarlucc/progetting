package com.progetting.shared;

import java.io.Serializable;
import java.util.Comparator;

public class Giudizio implements Serializable{
	
	private static final long serialVersionUID = -2073303690616097193L;
	
	protected int id;
	protected Integer voto;
	protected String giudice;
	protected int idRisposta;
	
    public Giudizio(){}
	
	public Giudizio(int id, Integer voto, String giudice, int idRisposta){
		
		this.id = id;
		this.voto = voto;
		this.giudice = giudice;
		this.idRisposta = idRisposta;
	}
	
	// METODI GET 
	
		public int getId(){
			  return this.id;
		}
		
		public Integer getVoto(){
			return this.voto;
		}
		
		public String getGiudice(){
			return this.giudice;
		}
		
		public int getIdRisposta(){
			return this.idRisposta;
		}


}
