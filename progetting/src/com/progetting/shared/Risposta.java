package com.progetting.shared;

import java.io.Serializable;


public class Risposta implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1721758874056155681L;
	
	
	protected int id;
	protected String text;
	protected String autore;
	protected String data;
	protected int idDomanda;
	protected Float mediaVoti;
	
	public Risposta(){}
	
	public Risposta(int id, String text, String autore, String data, int idDomanda){
		
		this.id = id;
		this.text = text;
		this.autore = autore;
		this.data = data;
		this.idDomanda = idDomanda;
	}

	// METODI GET 
	
	public int getId(){
		  return this.id;
		}
	
	public String getText(){
		return this.text;
	}
	
	public String getData(){
		return this.data;
	}
	
	public int getIdDomanda(){
		return this.idDomanda;
	}
	
	public String getAutore(){
		return this.autore;
	}
	
	public Float getMediaVoti(){
		return this.mediaVoti;
	}
	
	public void setMediaVoti(float media){
		this.mediaVoti = media;
	}
}
