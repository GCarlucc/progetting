package com.progetting.shared;

import java.io.Serializable;

public class Domanda implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1721758874056155681L;
	
	protected int id;
	protected String text;
	protected String autore;
	protected String data;
	protected String root;
	
	public Domanda(){}
	
	public Domanda(int id, String text, String autore, String data, String root){
		
		this.id = id;
		this.text = text;
		this.autore = autore;
		this.data = data;
		this.root = root;
	}

	// METODI GET 
	
	public int getId(){
		  return this.id;
		}
	
	public String getText(){
		return this.text;
	}
	
	public String getData(){
		return this.data;
	}
	
	public String getRoot(){
		return this.root;
	}
	
	public String getAutore(){
		return this.autore;
	}
	
	public void setRoot(String nuovaRoot){
		this.root = nuovaRoot;
	}
	
	
}
