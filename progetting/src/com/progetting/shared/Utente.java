package com.progetting.shared;

import java.io.Serializable;

public class Utente implements Serializable{

	private static final long serialVersionUID = -5326832533807295004L;

	private String username = "";
	private String password;
	private String email;
	private String nome;
	private String cognome;
	private String sesso;
	private String data;
	private String luogoNascita;
	private String domicilio;
	private boolean isGiudice;
	
	//costruttore vuoto
	public Utente(){}
	
	//costruttore utente
	public Utente(String username, String password, String email, String nome, String cognome, String sesso, String data, String luogoNascita, String domicilio, boolean isGiudice){
		this.username = username;
		this.password = password;
		this.email = email;
		this.nome = nome;
		this.cognome = cognome;
		this.sesso = sesso;
		this.data = data;
		this.luogoNascita = luogoNascita;
		this.domicilio = domicilio;
		this.isGiudice = isGiudice;
	}
	
	public Utente(String username, String password, String email, String nome, String cognome, String sesso, String data, String luogoNascita, String domicilio){
		
		this.username = username;
		this.password = password;
		this.email = email;
		this.nome = nome;
		this.cognome = cognome;
		this.sesso = sesso;
		this.data = data;
		this.luogoNascita = luogoNascita;
		this.domicilio = domicilio;
		
	}
	
	public String getUsername(){
		return this.username;
	}
	public void setUsername(String username){
		this.username = username;
	}
	
	public String getPassword(){
		return this.password;
	}
	public void setPassword(String password){
		this.password = password;
	}
	
	public String getEmail(){
		return this.email;
	}
	
	public boolean getIsGiudice(){
		return this.isGiudice;
	}
	public String getIsGiudiceToString(){
		String giudice;
		if(this.isGiudice == true){
			giudice = "true";
			return giudice;
		}
		else {
			giudice = "false";
			return giudice;
		}
	}
	public void setIsGiudice(boolean giudice){
		this.isGiudice = giudice;
	}

}
