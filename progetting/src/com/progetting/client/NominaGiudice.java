package com.progetting.client;

import java.util.ArrayList;

import com.github.gwtbootstrap.client.ui.Button;
import com.github.gwtbootstrap.client.ui.Fieldset;
import com.github.gwtbootstrap.client.ui.Label;
import com.github.gwtbootstrap.client.ui.constants.IconType;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.progetting.shared.Domanda;
import com.progetting.shared.Utente;

public class NominaGiudice {
	
	static Button nominaGiudice = new Button("Nomina Giudice");
	
	static void paginaGiudici(){
		
		nominaGiudice.setStyleName("btn btn-primary btn-large bottoneGiudice");
		nominaGiudice.setIcon(IconType.USER);
		
		nominaGiudice.addClickHandler(new ClickHandler(){

			@Override
			public void onClick(ClickEvent event) {
				
				caricaPagina();
				
			}});
	}

	static void caricaPagina(){
		
		RootPanel.get("sinistra").clear();
		RootPanel.get("destra").clear();
		RootPanel.get("contenuto").clear();
		nominaGiudice.setEnabled(false);
		InserimentoCategoria.insCategoria.setEnabled(true);
		RinominaCategoria.rinCategoria.setEnabled(true);
		
		// metodo restituisce tutti gli utenti
		Progetting.invocazione.restituisciUtenti(new AsyncCallback<ArrayList<Utente>>(){

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onSuccess(ArrayList<Utente> result) {
				
				VerticalPanel pagina = new VerticalPanel();
				
				
				for(int i = 0; i < result.size(); i++) 
				{
					// salta l'admin nelle label del setta giudice
					if( !result.get(i).getUsername().equalsIgnoreCase("admin")){
					
					Fieldset tab = new Fieldset();
					tab.setStyleName("tab");
					
					final String controlloGiudice;
					
					if(result.get(i).getIsGiudice() == true){
						controlloGiudice = "Utente Giudice"; 
					}else{
						controlloGiudice = "Utente NON Giudice";
					}
					
					final String user = result.get(i).getUsername();
					Label nomeutente = new Label(user);
					Label giudice = new Label(controlloGiudice);
					if(giudice.getText().equalsIgnoreCase("Utente Giudice")){
						giudice.setStyleName("labelgiudice");
					}
					else{
						giudice.setStyleName("labelnongiudice");
					}
					nomeutente.setStyleName("labelnomeut");
					Button status = new Button("cambia status");
					status.getElement().setId("buttonstatus");
					
					tab.add(nomeutente);
					tab.add(giudice);
					
					tab.add(status);
					
					
					pagina.add(tab);
					
					status.addClickHandler(new ClickHandler(){

						@Override
						public void onClick(ClickEvent event) {
							
							Progetting.invocazione.nominaGiudice(user, new AsyncCallback<Void>(){

								@Override
								public void onFailure(Throwable caught) {
									// TODO Auto-generated method stub
									Window.alert("Errore nel cambio status Giudice" + caught.getMessage());
								}

								@Override
								public void onSuccess(Void result) {
									// TODO Auto-generated method stub
									Window.alert("Status dell'utente " + user + " modificato");
									caricaPagina();
								}});
							
						}});
					
					RootPanel.get("contenuto").add(pagina);
					}
				}
			}});
		
		
	}
	
}
