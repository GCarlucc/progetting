package com.progetting.client;

import com.github.gwtbootstrap.client.ui.Label;
import com.github.gwtbootstrap.client.ui.TextBox;
import com.github.gwtbootstrap.client.ui.constants.IconType;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.regexp.shared.RegExp;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.github.gwtbootstrap.client.ui.Button;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TreeItem;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.progetting.shared.Categoria;

public class InserimentoCategoria {
	
	static String messaggioAlbero;
	static Button insCategoria = new Button("Inserisci Categoria");
	static Label title = new Label("Inserisci una categoria");
	
	static void inserimentoCategoria(){
		
		insCategoria.setStyleName("btn btn-primary btn-large inserimentoButton");
		insCategoria.setIcon(IconType.PLUS);
		insCategoria.addClickHandler(new ClickHandler(){

			@Override
			public void onClick(ClickEvent event) {
				// TODO Auto-generated method stub
				
				// Visibilità Pusanti
				RootPanel.get("destra").clear();
				RootPanel.get("contenuto").clear();
				RinominaCategoria.rinCategoria.setEnabled(true);
				NominaGiudice.nominaGiudice.setEnabled(true);
				
				insCategoria.setEnabled(false);
				final VerticalPanel insCat = new VerticalPanel();
				final HorizontalPanel panCat = new HorizontalPanel();
				final Button inviaCat = new Button("invia categoria");
				inviaCat.setStyleName("bottoniInvia");
				inviaCat.setIcon(IconType.OK_CIRCLE);
				title.getElement().setId("titleins");
				final Label testo = new Label();
				final TextBox testoNuovaCategoria = new TextBox();
				
				
				
				panCat.add(testo);
				panCat.add(testoNuovaCategoria);
				
				Albero.visualizzaAlbero();
				insCat.add(title);
				insCat.add(Albero.tree);
				insCat.add(Albero.labelMessageAlbero);
				Albero.labelMessageAlbero.setText("");
				
				
				Albero.tree.addSelectionHandler(new SelectionHandler<TreeItem>(){

					@Override
					public void onSelection(SelectionEvent<TreeItem> event) {
						// TODO Auto-generated method stub
						testoNuovaCategoria.setText(null);
						insCat.add(panCat);
						Albero.trovaPadre(event);
						
						if(Albero.labelMessageAlbero.getText().equalsIgnoreCase("categoria"))
						{
						  testo.setText("Inserisci la nuova categoria principale ed eventuali sub-category: ");
						  messaggioAlbero = "";
						}else{
						    messaggioAlbero = Albero.labelMessageAlbero.getText()+"/";
							testo.setText("Inserisci le sub-category di: " + messaggioAlbero);
							
							
						}
						insCat.add(inviaCat);	
					}
					
				});
	
				inviaCat.addClickHandler(new ClickHandler(){

					@Override
					public void onClick(ClickEvent event) {
						RegExp p = RegExp.compile("[^a-z0-9-/^A-Z ]");
						final boolean controlloContenuto = p.test(testoNuovaCategoria.getText());
						testoNuovaCategoria.setText(messaggioAlbero + testoNuovaCategoria.getText());
						if (controlloContenuto || testoNuovaCategoria.getText().endsWith("/") || testoNuovaCategoria.getText().contains("//")|| testoNuovaCategoria.getText().isEmpty())
						{
						   Window.alert("Ricontrolla la categoria ! Ammessi solo lettere, numeri e il carattere '/' per inserimento di sub-category ");
						   testoNuovaCategoria.setText(null);
						}else{
						    
							// se stringa va bene 
							Progetting.invocazione.creazioneIdCategoria(new AsyncCallback<Integer>(){
								
								@Override
								public void onFailure(Throwable caught) {
									// TODO Auto-generated method stub
									Window.alert("errore nella creazione Id della categoria" + caught.getMessage());
									
								}

								@Override
								public void onSuccess(Integer result) {
									final String testoCategoria = testoNuovaCategoria.getText();
									final int idCategoria = result;
									Progetting.invocazione.controlloCategorieTopDuplicate(testoCategoria, new AsyncCallback<Boolean>(){

										@Override
										public void onFailure(Throwable caught) {
											// TODO Auto-generated method stub
											Window.alert("errore nel controllo duplicati categorie" + caught.getMessage());
										}

										@Override
										public void onSuccess(Boolean result) {
											// TODO Auto-generated method stub
											
											if(!result){
												Categoria nuovaCategoria = new Categoria(idCategoria,testoCategoria);
												Progetting.invocazione.inserimentoCategoria(nuovaCategoria, new AsyncCallback<Void>(){

													@Override
													public void onFailure(Throwable caught) {
														// TODO Auto-generated method stub
														Window.alert("errore nell'inserimento della categoria" +caught.getMessage());
													}

													@Override
													public void onSuccess(Void result) {
														// TODO Auto-generated method stub
														Window.alert("Categoria inserita con Successo" );
														Home.aggiornamento();
												}});
											}
											else {
												Window.alert("Categoria già presente!" );
											}
											
										}
										
									});
									
									
									
									
								}});
						
						}
						
					}});
				
				RootPanel.get("destra").clear();
				RootPanel.get("contenuto").add(insCat);
			
			
			}});
	}

}
