package com.progetting.client;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;

public class Popolo {

	static void popolo(){
		// Popolo iniziale delle categorie di default
		
				Progetting.invocazione.popolocategorie(new AsyncCallback<Void>(){

					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						Window.alert("Errore del popolo categorie!!" + caught.getMessage());
					}

					@Override
					public void onSuccess(Void result) {
						// TODO Auto-generated method stub
						//ADMIN 
						Progetting.invocazione.inserimentoAdmin(new AsyncCallback<Void>(){

							@Override
							public void onFailure(Throwable caught) {
								Window.alert("errore admin");
								
							}

							@Override
							public void onSuccess(Void result) {
							
								VisualizzazioneDomanda.visualizzaDomanda();	
							}});
						
					}});
	}
}
