package com.progetting.client;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.github.gwtbootstrap.client.ui.Button;
import com.github.gwtbootstrap.client.ui.constants.IconType;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.progetting.shared.Utente;

public class Registrazione {
	
	final static VerticalPanel registrazionePnl = new VerticalPanel();
	final static Label username = new Label("Inserire il nome utente (OBBLIGATORIO)");
	static TextBox txtUsername = new TextBox();
	final static Label password = new Label("Inserisci la password (OBBLIGATORIO)");
	static PasswordTextBox txtPassword = new PasswordTextBox();
	final static Label email = new Label("Inserisci l'indirizzo e-mail (OBBLIGATORIO)");
	static TextBox txtEmail = new TextBox();
	final static Label nome = new Label("Inserisci il nome ");
	final static TextBox txtNome = new TextBox();
	final static Label cognome = new Label("Inserisci il cognome ");
	final static TextBox txtCognome = new TextBox();
	final static Label sesso = new Label("Inserisci il sesso ");
	final static ListBox lstSesso=new ListBox();
	final static Label data = new Label("Inserisci la tua data di nascita");
	final static TextBox txtData = new TextBox();
	final static Label luogonascita = new Label("Inserisci il tuo luogo di nascita ");
	final static TextBox txtLuogonascita = new TextBox();
	final static Label domicilio = new Label("Inserisci il tuo luogo di domicilio ");
	final static TextBox txtDomicilio = new TextBox();
	static Button btnregistrazione = new Button("Registrati");
	
	static Button registrati = new Button("Registrati");
	
	
	
	static void caricaregistrazione(){
		
		//Style
		lstSesso.clear();
		lstSesso.addItem("Uomo", "0");
		lstSesso.addItem("Donna", "1");
		lstSesso.setSelectedIndex(-1);
		btnregistrazione.setStyleName("bottoniInvia");
		btnregistrazione.setIcon(IconType.KEY);
		username.setStyleName("defaultLabel");
		password.setStyleName("defaultLabel");
		email.setStyleName("defaultLabel");
		nome.setStyleName("defaultLabel");
		cognome.setStyleName("defaultLabel");
		sesso.setStyleName("defaultLabel");
		data.setStyleName("defaultLabel");
		luogonascita.setStyleName("defaultLabel");
		domicilio.setStyleName("defaultLabel");
		
		//Costruisco il Pannello
		
		registrazionePnl.add(username);
		registrazionePnl.add(txtUsername);
		registrazionePnl.add(password);
		registrazionePnl.add(txtPassword);
		registrazionePnl.add(email);
		registrazionePnl.add(txtEmail);
		registrazionePnl.add(nome);
		registrazionePnl.add(txtNome);
		registrazionePnl.add(cognome);
		registrazionePnl.add(txtCognome);
		registrazionePnl.add(sesso);
		registrazionePnl.add(lstSesso);
		registrazionePnl.add(data);
		registrazionePnl.add(txtData);
		registrazionePnl.add(luogonascita);
		registrazionePnl.add(txtLuogonascita);
		registrazionePnl.add(domicilio);
		registrazionePnl.add(txtDomicilio);
		registrazionePnl.add(btnregistrazione);
		registrazionePnl.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
		
		//Carichiamo il pannello di Registrazione 
		
	    RootPanel.get("contenuto").add(registrazionePnl);
		
	}
	
	//controllo l'inserimento dei campi obbligatori
	public static boolean insObbligatorio(){
		
		if(txtUsername.getText() == "" || txtPassword.getText() == "" || txtEmail.getText() == "" ){
			
			return false;
		}
		else{
				return true;	
			}	
		
	}
	
	//setto i campi a vuoti, da riguardare dopo aggiunata di indirizzamento
	static void settoCampi(){
		
		txtUsername.setText("");
		txtPassword.setText("");
		txtEmail.setText("");
		txtNome.setText("");
		txtCognome.setText("");
		txtData.setText("");
		txtLuogonascita.setText("");
		txtDomicilio.setText("");
		lstSesso.setSelectedIndex(-1);
	}
   //Richiamiamo nel div "contenuto" la ui della registrazione	
   static void effettuaRegistrazione(){
	 
		
	 		registrati.addClickHandler(new ClickHandler(){
	 			public void onClick(ClickEvent event){
	 				
	 				RootPanel.get("contenuto").clear();
	 				RootPanel.get("menu").remove(Progetting.pannelloCentrale);
	 				Registrazione.caricaregistrazione();	
	 			}
	 			
	 		});
   }
   
   // evento registrazione lanciato dal bottone Registrati (con controllo dei campi) che immette il nuovo utente nel database
   static void eventoRegistrazione(){
	   btnregistrazione.addClickHandler(new ClickHandler(){
			public void onClick(ClickEvent event){
				
				String nomeU = txtUsername.getText();
				String emailU = txtEmail.getText();
		
				Progetting.invocazione.controllaRegUtente(nomeU, new AsyncCallback<Boolean>(){

					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						Window.alert("Errore nel controllo della registrazione utente!" + caught.getMessage());
					}

					@Override
					public void onSuccess(Boolean result) {
						// TODO Auto-generated method stub
						
						if(result == true){
							Window.alert("username già presente");
						}
						
						else{	
							String nomeUser = txtUsername.getText();
							String pass = txtPassword.getText();
							String emailU = txtEmail.getText();
							String nomeU = txtNome.getValue();
							String cognomeU  = txtCognome.getValue();
							String sessoU = lstSesso.getSelectedItemText();
							String dataU = txtData.getValue();
							String luogonascitaU = txtLuogonascita.getValue();
							String domicilioU = txtDomicilio.getValue();
							
							if(insObbligatorio()==true){
								
								Utente nuovoUtente = new Utente (nomeUser,pass,emailU,nomeU,cognomeU,sessoU,dataU,luogonascitaU,domicilioU);
								Progetting.invocazione.registrazioneUtente(nuovoUtente, new AsyncCallback<Void>(){

									@Override
									public void onFailure(Throwable caught) {
										// TODO Auto-generated method stub
										Window.alert("Fallimento registrazione! " + caught.getMessage() );
									}

									@Override
									public void onSuccess(Void result) {
										Window.alert("registrazione avvenuta con successo !");
										settoCampi();
										Window.Location.reload(); // torno alla home di partenza e azzero
									}});
								
							}
							else{
								Window.alert("non hai inserito tutti i campi obbligatori");
							}
							
						}
					}
				});
			}
			
		});
   }
	

}
