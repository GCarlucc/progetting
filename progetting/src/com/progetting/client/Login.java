package com.progetting.client;

import java.util.ArrayList;

import com.github.gwtbootstrap.client.ui.constants.IconType;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.regexp.shared.MatchResult;
import com.google.gwt.regexp.shared.RegExp;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.github.gwtbootstrap.client.ui.Button;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.TreeItem;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.progetting.shared.Categoria;
import com.progetting.shared.Domanda;
import com.progetting.shared.Utente;

public class Login {
	
	static VerticalPanel loginPnl = new VerticalPanel();
	static HorizontalPanel pannelloUser = new HorizontalPanel();
	static Label user = new Label();
	static Label username = new Label("Inserisci l'username");
	static Label password = new Label("Inserisci la password");
	static TextBox txtUsername = new TextBox();
	static PasswordTextBox txtPassword = new PasswordTextBox();
	static Button btnlogin = new Button("Login");
	static Button login = new Button("Login");
	
	static Utente loginUser = new Utente(); // per tenere traccia dell'utente anche durante inserimento di domande e risposte
	
	 static void caricaLogin(){
		
		//Style
		username.setStyleName("defaultLabel");
		password.setStyleName("defaultLabel");
		btnlogin.setStyleName("bottoniInvia");
		btnlogin.setIcon(IconType.SIGNIN);
		
		
		//Costruisco Pannello
		loginPnl.add(username);
		loginPnl.add(txtUsername);
		loginPnl.add(password);
		loginPnl.add(txtPassword);
		loginPnl.add(btnlogin);
		
		//Carichiamo il pannello di Login 
		
		RootPanel.get("contenuto").add(loginPnl);
	}
	 
		
	 //Richiamiamo nel div "contenuto" la ui del login
		static void effettuaLogin(){
			login.addClickHandler(new ClickHandler(){
				public void onClick(ClickEvent event){
					RootPanel.get("contenuto").clear();
					RootPanel.get("menu").remove(Progetting.pannelloCentrale);
					loginPnl.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
					Login.caricaLogin();	
				}	
			});
		}
		
		// gestione dell'evento di login dopo il click sul bottone per controllare username e password inseriti e convalidare (o no) l'accesso
		static void eventoLogin(){
			
			 btnlogin.addClickHandler(new ClickHandler(){
					public void onClick(ClickEvent event){
						
					    loginUser.setUsername(txtUsername.getText());
						loginUser.setPassword(txtPassword.getText());
						
						Progetting.invocazione.effettuaLogin(loginUser.getUsername(),loginUser.getPassword(), new AsyncCallback<Utente>(){
							
							@Override
							public void onFailure(Throwable caught) {
								
									// Error
									Window.alert("utente non trovato ! " + caught.getMessage());
									
								}
								

							@Override
							public void onSuccess(Utente result) {
								if (result != null) {
								Window.alert("Login effettuato con successo !");
								loginUser = result;
								// TODO Cosa succede una volta che l'utente si logga con successo (1090)
								if(loginUser.getUsername().equalsIgnoreCase("admin")){
									Admin.caricaAdmin();
								} //fine if admin
								else {
									Registrazione.registrati.setVisible(false);
									login.setVisible(false);
									InserimentoDomanda.insDomanda.setVisible(true);
									Logout.logout.setVisible(true);
								}
								 
								// login comune admin e utente per albero e domande
								 Progetting.pannelloAffiancato.add(Albero.tree);
								 Progetting.pannelloAffiancato.add(Albero.labelMessageAlbero);
								 user.setText(loginUser.getUsername());
								 pannelloUser.add(user);
								 RootPanel.get("loggedUser").add(pannelloUser);
								 Albero.visualizzaAlbero();
								 
								 Albero.tree.addSelectionHandler(new SelectionHandler<TreeItem>() {
							         public void onSelection(SelectionEvent<TreeItem> event) {
							            Albero.labelMessageAlbero.setText( event.getSelectedItem().getText());
//							            final String rootDomanda = Albero.labelMessageAlbero.getText();
							            VisualizzazioneDomanda.visualizzaDomanda();
							         }
							      });
								 
								 // comuni ad admin e utente
								 RootPanel.get("contenuto").clear();
								 RootPanel.get("destra").add(Progetting.pannelloCentrale);
								 RootPanel.get("sinistra").add(Progetting.pannelloAffiancato);
								 
								 
								 
							  }else{
								  Window.alert("Nome utente o Password errati !");
							  }
							}


							
							
						});
					}	
				});
		}
  }


