package com.progetting.client;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.github.gwtbootstrap.client.ui.Button;
import com.github.gwtbootstrap.client.ui.constants.IconType;
import com.google.gwt.user.client.ui.RootPanel;

public class Logout {
static Button logout = new Button("Logout");
	
  // Avvenimento di Logout !
  static void tastoLogout()
	{
	  	logout.setIcon(IconType.SIGNOUT);
		logout.addClickHandler(new ClickHandler()
		{

			@Override
			public void onClick(ClickEvent event) 
			{
				Window.Location.reload();	
			}
			
		});
	}


}
