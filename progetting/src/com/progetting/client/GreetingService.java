package com.progetting.client;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.progetting.shared.Categoria;
import com.progetting.shared.Domanda;
import com.progetting.shared.Giudizio;
import com.progetting.shared.Risposta;
import com.progetting.shared.Utente;

/**
 * The client-side stub for the RPC service.
 */
@RemoteServiceRelativePath("greet")
public interface GreetingService extends RemoteService {

	
	
	Utente effettuaLogin(String username, String password) throws IllegalArgumentException;

	void registrazioneUtente(Utente nuovoUtente) throws IllegalArgumentException;
	
	boolean controllaRegUtente(String username) throws IllegalArgumentException;
	
	void popolocategorie() throws IllegalArgumentException; 

	ArrayList<String> visualizzaCategoria() throws IllegalArgumentException;

	void inserimentoDomanda(Domanda nuovaDomanda) throws IllegalArgumentException;

	int creazioneIdDomanda() throws IllegalArgumentException;

	ArrayList<Domanda> visualizzaDomanda() throws IllegalArgumentException;

	void inserimentoRisposta(Risposta nuovaRisposta) throws IllegalArgumentException;

	ArrayList<Risposta> visualizzaRisposta() throws IllegalArgumentException;

	int creazioneIdRisposta() throws IllegalArgumentException;

	void inserimentoAdmin() throws IllegalArgumentException;

	void nominaGiudice(String username);

	void eliminaDomanda(int id) throws IllegalArgumentException;

	int creazioneIdCategoria() throws IllegalArgumentException;

	void inserimentoCategoria(Categoria nuovaCategoria) throws IllegalArgumentException;

	int creazioneIdGiudizio() throws IllegalArgumentException;

	void inserimentoGiudizio(Giudizio nuovoGiudizio) throws IllegalArgumentException;

	boolean seGiudicata(String autoreGiudizio, int idRisposta) throws IllegalArgumentException;
	
	void rinominaCategoria(String nomeVecchio, String cambioNome) throws IllegalArgumentException;

	ArrayList<Utente> restituisciUtenti() throws IllegalArgumentException;

	ArrayList<String> cercaGiudici(int idRisposta) throws IllegalArgumentException;

	void eliminaRisposta(int idRisposta) throws IllegalArgumentException;

	boolean controlloCategorieTopDuplicate(String inserita) throws IllegalArgumentException;

	//Float calcolaMediaGiudizi(int idRisposta) throws IllegalArgumentException;

	//ArrayList<Giudizio> ordinaGiudizi() throws IllegalArgumentException;
}


