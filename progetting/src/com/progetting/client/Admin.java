package com.progetting.client;

import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.RootPanel;

public class Admin {
	
	static HorizontalPanel pannelloAdmin = new HorizontalPanel();

       static void caricaAdmin(){
    	   
    	   pannelloAdmin.setStyleName("pannello");
    	   
    	 //costruisco pannello Admin
			pannelloAdmin.add(InserimentoCategoria.insCategoria);
			pannelloAdmin.add(RinominaCategoria.rinCategoria);
			pannelloAdmin.add(Home.home);
			pannelloAdmin.add(NominaGiudice.nominaGiudice);
			pannelloAdmin.add(Logout.logout);
			Logout.logout.setVisible(true);
			
			//Inserimento Categoria
			InserimentoCategoria.inserimentoCategoria();
			
			//Rinomina Categoria
			RinominaCategoria.rinominaCategoria();
			
			//Nomina Giudice
			NominaGiudice.paginaGiudici();
			
			// abilita elimina fin dal primo accesso admin
			VisualizzazioneDomanda.visualizzaDomanda();
			RootPanel.get("menu").clear();
			RootPanel.get("contenuto").clear();
			RootPanel.get("adminMenu").add(pannelloAdmin);
    	   
       }
}
