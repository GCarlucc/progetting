package com.progetting.client;



import java.util.Date;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.github.gwtbootstrap.client.ui.Button;
import com.github.gwtbootstrap.client.ui.constants.IconType;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.progetting.shared.Domanda;

public class InserimentoDomanda {
	
	static VerticalPanel insPnl = new VerticalPanel();
	static Label domanda = new Label("Inserisci il testo della domanda");
	static Label categ = new Label("Seleziona la categoria");
	static TextArea txtDomanda = new TextArea();
    static Button submit = new Button(" Invia");
    
    static Button insDomanda = new Button ("Inserisci Domanda");
    	
	
	public static void caricaInserimentoDomanda(){
		
		//Progetting x = new Progetting();
		Albero.visualizzaAlbero();
		domanda.getElement().setId("titoloInsDomanda");
		categ.getElement().setId("titoloSelCategoria");
		txtDomanda.getElement().setId("textArea");
		submit.setStyleName("bottoniInvia");
		submit.setIcon(IconType.OK_CIRCLE);
		
		//Costruisco Pannello
		insPnl.clear();
		insPnl.add(domanda);
	    txtDomanda.getElement().setAttribute("maxlength","300");
	    txtDomanda.getElement().setAttribute("placeholder","max 300 caratteri...");
	    txtDomanda.getElement().setAttribute("rows","4");
	    txtDomanda.getElement().setAttribute("cols","150");
		txtDomanda.setText("");
		insPnl.add(txtDomanda);
		insPnl.add(categ);
		Albero.labelMessageAlbero.setText("");
        insPnl.add(Albero.labelMessageAlbero);
        insPnl.add(Albero.tree);
	    insPnl.add(submit);
	    
	  //Carichiamo il pannello di Inserimento Domanda 
	    
	    RootPanel.get("contenuto").add(insPnl);
	}
	
	//controllo degli inserimenti
	public static boolean insObbligatorio(){
		
		if(txtDomanda.getText() == "" || Albero.labelMessageAlbero.getText() == ""){
			
			return false;
		}
		else{
				return true;	
			}	
		
	}
	
	static void effettuaInsDomanda(){
		insDomanda.addClickHandler(new ClickHandler(){
			public void onClick(ClickEvent event){
				
				insDomanda.setEnabled(false);
				RootPanel.get("destra").clear();
				RootPanel.get("sinistra").clear();
				RootPanel.get("contenuto").clear();
				caricaInserimentoDomanda();
			}
		});
	}
	
	// evento che gestisce l'inserimento della domanda dopo averla scritta e scelto la categoria
	static void eventoInsDomanda(){
		submit.addClickHandler(new ClickHandler(){
			public void onClick(ClickEvent event){
				Progetting.invocazione.creazioneIdDomanda(new AsyncCallback<Integer>(){

					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						Window.alert("Fallimento nella creazione Id Domanda " + caught.getMessage() );
						
					}

					@Override
					public void onSuccess(Integer result) {
						if(insObbligatorio() == false){
							
						Window.alert("Non hai selezionato la categoria o non hai inserito il testo della domanda !!");
						
						}
						else{
							
							int id = result;
							String testo = txtDomanda.getText();
							String autore = Login.loginUser.getUsername();
							String root = Albero.labelMessageAlbero.getText();
							Date now = new Date();
							String dataCorrente = DateTimeFormat.getMediumDateTimeFormat().format(now);
							Domanda nuovaDomanda = new Domanda(id,testo,autore,dataCorrente,root);
							Progetting.invocazione.inserimentoDomanda(nuovaDomanda, new AsyncCallback<Void>(){

								@Override
								public void onFailure(Throwable caught) {
									Window.alert("Errore Durante l'iserimento della Domanda !" + caught.getMessage() );
								
								}

								@Override
								public void onSuccess(Void result) {
									// TODO Auto-generated method stub
									Window.alert("Domanda inserita con successo !");
									/*
									RootPanel.get("contenuto").clear();
								
									Albero.visualizzaAlbero();
									Albero.labelMessageAlbero.setText(null);
									Progetting.pannelloAffiancato.add(Albero.tree);
									Progetting.pannelloAffiancato.add(Albero.labelMessageAlbero);
									RootPanel.get("destra").add(Progetting.pannelloCentrale);
									RootPanel.get("sinistra").add(Progetting.pannelloAffiancato);
									insDomanda.setEnabled(true);
									VisualizzazioneDomanda.visualizzaDomanda();
                                 */
									Home.aggiornamento();
								}
							}); // fine inserimentoDomanda
						}
					} // fine onSuccess
				}); // fine creazioneIdDomanda
			} // fine OnClick
		}); // fine ClickHandler
	} // fine metodo eventoInsDomanda
}
