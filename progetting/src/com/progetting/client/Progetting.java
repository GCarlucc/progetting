package com.progetting.client;

import com.progetting.shared.Domanda;
import com.progetting.shared.FieldVerifier;
import com.progetting.shared.Utente;


import java.util.ArrayList;
import java.util.Date;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.github.gwtbootstrap.client.ui.Button;
import com.github.gwtbootstrap.client.ui.constants.IconType;
import com.google.gwt.user.client.ui.DecoratorPanel;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Tree;
import com.google.gwt.user.client.ui.TreeItem;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class Progetting implements EntryPoint {
	//A.A.CERCASI
	//*********
	static HorizontalPanel pannello = new HorizontalPanel();
	static VerticalPanel pannelloCentrale = new VerticalPanel();
	static VerticalPanel pannelloAffiancato = new VerticalPanel();
	static HorizontalPanel pannelloTitolo = new HorizontalPanel();
	static Label titolo = new Label("Benvenuto in Pho-Rum");
	
	
	/**
	 * The message displayed to the user when the server cannot be reached or
	 * returns an error.
	 */
	private static final String SERVER_ERROR = "An error occurred while "
			+ "attempting to contact the server. Please check your network " + "connection and try again.";

	/**
	 * Create a remote service proxy to talk to the server-side Greeting service.
	 */
	final static GreetingServiceAsync invocazione = GWT.create(GreetingService.class);

	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {
		
		// Home clickHandeler

		
		//Style 
		
		titolo.getElement().setId("titolo");
		Registrazione.registrati.setStyleName("btn btn-primary btn-large registrati");
		Registrazione.registrati.setIcon(IconType.PENCIL);
		Login.pannelloUser.getElement().setId("pannellouser");
		Login.user.getElement().setId("labeluser");
		Login.login.setStyleName("btn btn-primary btn-large login");
		Login.login.setIcon(IconType.HAND_RIGHT);
		Home.home.setStyleName("btn btn-success btn-large home");
		Home.home.setIcon(IconType.HOME);
		Logout.logout.setStyleName("btn btn-danger btn-large logout");
		pannello.setStyleName("pannello");
		InserimentoDomanda.insDomanda.setStyleName("btn btn-primary btn-large");
		InserimentoDomanda.insDomanda.setIcon(IconType.EDIT);
		
		// Assembliamo l'Horizontal Panel

		pannello.add(Registrazione.registrati);
		pannello.add(Home.home);
		pannello.add(Login.login);
		pannello.add(InserimentoDomanda.insDomanda);
		pannello.add(Logout.logout);
		pannelloTitolo.add(titolo);
		InserimentoDomanda.insDomanda.setVisible(false);
		Logout.logout.setVisible(false);
		
		
		//Gestione evento del pulsante Home
		Home.tastoHome();
		
		//Gestione evento del pulsante Logout
		Logout.tastoLogout();
		
		//Popolamento Categorie Base e visualizzazione domande per Utente non Loggato
		Popolo.popolo();
		
		//Visualizzazione Albero Categorie
		Albero.visualizzaAlbero();
		
		//ClickHandler di Login
        Login.effettuaLogin();
		
        //ClickHandler di Registrazione
        Registrazione.effettuaRegistrazione();
		
		//Richiamiamo nel div "contenuto" la ui dell' inserimento domanda
		InserimentoDomanda.effettuaInsDomanda();
		
		// gestione dell'evento di login dopo il click sul bottone per controllare username e password inseriti e convalidare (o no) l'accesso
		// visualizza domande in base alla scelta di categoria
		Login.eventoLogin();
		
		// evento registrazione lanciato dal bottone Registrati (con controllo dei campi) che immette il nuovo utente nel database
		Registrazione.eventoRegistrazione();
		
		// evento che gestisce l'inserimento della domanda dopo averla scritta e scelto la categoria
		InserimentoDomanda.eventoInsDomanda();
		
		// evento che gestisce l'inserimento della risposta dopo averla scritta
		VisualizzazioneDomanda.eventoInsRisposta();
		//VisualizzazioneDomanda.visualizzaRisposte();
		
		
		RootPanel.get("loggedUser").add(pannelloTitolo);
		// per utente non loggato
		RootPanel.get("menu").add(pannello);
		RootPanel.get("menu").add(pannelloCentrale);
		
	
	} // si chiude On ModuleLoad()
	
} // si chiude l'Entry Point


