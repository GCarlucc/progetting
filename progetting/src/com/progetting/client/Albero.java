package com.progetting.client;

import java.util.ArrayList;

import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Tree;
import com.google.gwt.user.client.ui.TreeItem;
import com.google.gwt.user.client.ui.VerticalPanel;

public class Albero {
	
	static Label labelMessageAlbero = new Label();
	public static Tree tree = new Tree();
	
	
	//===========================================================================//
	
	// Array di appoggio per gestione categorie nell'albero !
	public static boolean controllo(ArrayList<String> arr , String par){
    	
  	  for(int i = 0; i<arr.size(); i++){
  		  if(arr.get(i).equalsIgnoreCase(par)){
  			  return true;
  		  }
  	  }
  	  return false;  
    }
	
	public static boolean controlloSubCat(ArrayList<String> arr , String par){
		for(int i = 0; i<arr.size(); i++){
  		  if(arr.get(i).toLowerCase().contains(par.toLowerCase())){
  			  return true;
  		  }
	  	}
		return false;
	}
	
	
	// CREO IL METODO PER LA CREAZIONE DELLE CATEGORIE
	public synchronized static void visualizzaAlbero(){
		Progetting.invocazione.visualizzaCategoria(new AsyncCallback<ArrayList<String>>(){

			@Override
			public void onFailure(Throwable caught) 
			{
				// TODO Auto-generated method stub
				Window.alert("Errore nel visualizza Albero ! " + caught.getMessage());	
			}
			
			

			@Override
			public void onSuccess(ArrayList<String> result) 
			{
				// TODO Auto-generated method stub
				
				tree.setStyleName("albero");
				labelMessageAlbero.getElement().setId("catSelezionata");
				
				TreeItem categoria = new TreeItem();
			      categoria.setText("categoria");
			      labelMessageAlbero.setWidth("300");
			      ArrayList<String> contenuto = new ArrayList<String>();

					//la pulizia serve per evitare il popolamento continuo delle stesse categorie quando premo ripetutamente il popolamento
				    tree.clear();
				    for(int i =0; i<result.size(); i++){
				    	
					    	TreeItem item = new TreeItem();
					    	item.setStyleName("categorieTop");
					    	String car = result.get(i);
					    	if( car.contains("/") == false)
					    	 {
					    		item.setText(car);

					    		if(controllo(contenuto,car) == false){
					    			categoria.addItem(item);
							    	contenuto.add(car);
					    		}

						    	if(controllo(contenuto,car) == false)
						    	{
					    		  categoria.addItem(item);
						      contenuto.add(car);
						    	}
					    	 }
					    	else
					    	 {
					    			String [] cc = car.split("/");
					    			item.setText(cc[0]);
					    			
					    			if(controllo(contenuto,cc[0]) == false)
					    			{
					    			  categoria.addItem(item);
					    			  contenuto.add(cc[0]);
					    	        }
					    			else
					    			{
					    			for ( int w = 0; w<= categoria.getChildCount() - 1 ; w++) 
					    			  {
					    				 if(categoria.getChild(w).getText().equalsIgnoreCase(cc[0]))
					    				 {
					    					 item = categoria.getChild(w);
					    				 }
					    			   }
					    			}
					    			String sub = cc[0];
					    			for(int x = 1; x<cc.length;x++)
					    			{
					    					sub = sub + "/" + cc[x];
					    					TreeItem figlio = new TreeItem();
					    					figlio.setText(cc[x]);//x=1 cane
					    					
					    					if(controlloSubCat(contenuto,cc[x]) == false)
					    					{
					    						item.addItem(figlio);//assegna cane ad animale
						    					contenuto.add(sub);
					    					}
					    					else
					    					{
					    						String newCat = cc[x-1] + "/" + cc[x];
					    						if(controlloSubCat(contenuto,newCat) == false)
					    						{
					    							item.addItem(figlio);
							    					contenuto.add(car);
					    						}
					    					}
					    					
					    					for(int l=0; l<= item.getChildCount() - 1 ; l++)
					    					{
					    						if(item.getChild(l).getText().equalsIgnoreCase(cc[x]))
					    						{
					    							item = item.getChild(l);
					    						}
					    					}
					    						
					    			}
					    			
					    			
					    	}
					    	
						}
				    	
				    	tree.addItem(categoria);
				    
				    	tree.addSelectionHandler(new SelectionHandler<TreeItem>() 
				    	{
					         public void onSelection(SelectionEvent<TreeItem> event) 
					         {
					             trovaPadre(event);
					         }
					     });	
			}
			});
		
	} // fine metodo visualizzaAlbero
	
	public static void trovaPadre(SelectionEvent<TreeItem> evento){
		String father = evento.getSelectedItem().getText();
   	 
   	 	if(!father.equalsIgnoreCase("categoria")){
   	 		TreeItem padre = evento.getSelectedItem().getParentItem(); 
   	 		while(!padre.getText().equalsIgnoreCase("categoria"))
   	 		{
	        	 father = padre.getText() + "/" + father;
	        	 padre = padre.getParentItem();
   	 		}  
   	 	}
   	 
        labelMessageAlbero.setText( father );
	}

	
}