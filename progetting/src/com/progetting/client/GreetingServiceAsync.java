package com.progetting.client;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.progetting.shared.Categoria;
import com.progetting.shared.Domanda;
import com.progetting.shared.Giudizio;
import com.progetting.shared.Risposta;
import com.progetting.shared.Utente;

/**
 * The async counterpart of <code>GreetingService</code>.
 */
public interface GreetingServiceAsync {

	void effettuaLogin(String username, String password, AsyncCallback<Utente> callback);

	void registrazioneUtente(Utente nuovoUtente, AsyncCallback<Void> callback) throws IllegalArgumentException;

	void controllaRegUtente(String username, AsyncCallback<Boolean> callback) throws IllegalArgumentException;

	void popolocategorie(AsyncCallback<Void> callback) throws IllegalArgumentException;

	void visualizzaCategoria(AsyncCallback<ArrayList<String>> callback);

	void inserimentoDomanda(Domanda nuovaDomanda, AsyncCallback<Void> callback);

	void creazioneIdDomanda(AsyncCallback<Integer> callback);

	void visualizzaDomanda(AsyncCallback<ArrayList<Domanda>> callback);

	void inserimentoRisposta(Risposta nuovaRisposta, AsyncCallback<Void> callback);

	void visualizzaRisposta(AsyncCallback<ArrayList<Risposta>> callback);

	void creazioneIdRisposta(AsyncCallback<Integer> callback);

	void inserimentoAdmin(AsyncCallback<Void> callback);

	void nominaGiudice(String username, AsyncCallback<Void> callback);

	void eliminaDomanda(int id, AsyncCallback<Void> callback);

	void creazioneIdCategoria(AsyncCallback<Integer> callback);

	void inserimentoCategoria(Categoria nuovaCategoria, AsyncCallback<Void> callback);

	void creazioneIdGiudizio(AsyncCallback<Integer> callback);

	void inserimentoGiudizio(Giudizio nuovoGiudizio, AsyncCallback<Void> callback);

	void seGiudicata(String autoreGiudizio, int idRisposta, AsyncCallback<Boolean> callback);

	void rinominaCategoria(String nomeVecchio, String cambioNome, AsyncCallback<Void> callback);

	void restituisciUtenti(AsyncCallback<ArrayList<Utente>> callback);

	void cercaGiudici(int idRisposta, AsyncCallback<ArrayList<String>> callback);

	void eliminaRisposta(int idRisposta, AsyncCallback<Void> callback);

	void controlloCategorieTopDuplicate(String inserita, AsyncCallback<Boolean> callback);

	//void calcolaMediaGiudizi(int idRisposta, AsyncCallback<Float> callback);

//	void ordinaGiudizi(AsyncCallback<ArrayList<Giudizio>> callback);
	
}



