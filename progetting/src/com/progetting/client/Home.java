package com.progetting.client;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.github.gwtbootstrap.client.ui.Button;
import com.google.gwt.user.client.ui.RootPanel;

public class Home {

	static Button home = new Button("Home");
	
	static void tastoHome(){
		home.addClickHandler(new ClickHandler(){

			@Override
			public void onClick(ClickEvent event) {
				
				if( Login.loginUser.getUsername().isEmpty()){
					Window.Location.reload();
				}else{
					aggiornamento();
					
				}
				
			}});
	}
	
	static void aggiornamento(){
		
		RootPanel.get("contenuto").clear();
		Albero.visualizzaAlbero();
		Albero.labelMessageAlbero.setText(null);
		Progetting.pannelloAffiancato.add(Albero.tree);
		Progetting.pannelloAffiancato.add(Albero.labelMessageAlbero);
		RootPanel.get("destra").add(Progetting.pannelloCentrale);
		RootPanel.get("sinistra").add(Progetting.pannelloAffiancato);
		InserimentoDomanda.insDomanda.setEnabled(true);
		InserimentoCategoria.insCategoria.setEnabled(true);
		RinominaCategoria.rinCategoria.setEnabled(true);
		NominaGiudice.nominaGiudice.setEnabled(true);
		VisualizzazioneDomanda.visualizzaDomanda();
	}
}
