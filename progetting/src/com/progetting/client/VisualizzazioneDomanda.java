package com.progetting.client;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

import com.github.gwtbootstrap.client.ui.Button;
import com.github.gwtbootstrap.client.ui.Fieldset;
import com.github.gwtbootstrap.client.ui.ListBox;
import com.github.gwtbootstrap.client.ui.TextArea;
import com.github.gwtbootstrap.client.ui.constants.IconType;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.DecoratedPopupPanel;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ToggleButton;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.progetting.shared.Domanda;
import com.progetting.shared.Giudizio;
import com.progetting.shared.Risposta;

public class VisualizzazioneDomanda {
	
	
	 // elementi dialogBox
	static DialogBox dialogBox = new DialogBox();
	static Label testoDomanda = new Label();
	static Label autoreDomanda = new Label();
	static Label dataDomanda = new Label();
	static Label catDomanda = new Label();
	static Integer idDomanda;
	static Integer idRisposta;
	static VerticalPanel dialogPanel = new VerticalPanel();
	static Button inserisciRisposta = new Button("Rispondi alla Domanda");
	static Button annulla = new Button("Chiudi");
	static TextArea textArea = new TextArea();
	static String votoSelezionato = new String();
	static DecoratedPopupPanel popupGiudici;
	
	
	public static boolean insObbligatorio(){
		
		if(textArea.getText() == ""){
			return false;
		}
		else{
			return true;	
		}	
			
	}
	
	public static void dialog(){
		
       // dialogBox
		
		dialogBox.setAnimationEnabled(true);
		dialogBox.setAnimationEnabled(true);
		dialogBox.setText("Risposte");
		inserisciRisposta.setStyleName("buttonDialog");
		inserisciRisposta.setIcon(IconType.OK);
		testoDomanda.getElement().setId("testoDomanda");
		textArea.getElement().setAttribute("placeholder", "Scrivi qui la tua risposta...");
		dataDomanda.getElement().setId("dataDomanda");
		catDomanda.getElement().setId("catDomanda");
		annulla.setStyleName("buttonDialog");
		annulla.setIcon(IconType.REMOVE);
		// Pannello usato nel DialogBox
		
		dialogPanel.setHeight("200px");
		dialogPanel.setWidth("550px");
		dialogPanel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER); //allineamento centrale
		dialogPanel.add(testoDomanda);
		dialogPanel.add(autoreDomanda);
		dialogPanel.add(dataDomanda);
		dialogPanel.add(catDomanda);
		//ClickHandler del pulsante "ok" dentro al dialogBox
		
		if(Login.loginUser.getUsername().isEmpty()){
			dialogPanel.add(annulla);
		}
		annulla.addClickHandler(new ClickHandler() 
        {
            
            public void onClick(ClickEvent event) 
            {
	            	dialogPanel.clear();	
	            	dialogBox.hide();
	            	popupGiudici.hide();
            	
            }
         });
	
	}
	
	//evento Inserimento della risposta
	static void eventoInsRisposta(){
		inserisciRisposta.addClickHandler(new ClickHandler() 
		{
            public void onClick(ClickEvent event) 
            {
            	Progetting.invocazione.creazioneIdRisposta(new AsyncCallback<Integer>(){
            					
            		@Override
            		public void onFailure(Throwable caught) {
            			// TODO Auto-generated method stub
            			Window.alert("Fallimento nella creazione Id Risposta " + caught.getMessage() );
            						
            		}
            					
            		@Override
            		public void onSuccess(Integer result) {
            			if(insObbligatorio() == false){
            				Window.alert("non hai inserito la risposta!");
            			}
            			else {
            							
            				int id = result;
            				String testo = textArea.getText();
            				String autore = Login.loginUser.getUsername();
            				Date now = new Date();
            				String dataCorrente = DateTimeFormat.getMediumDateTimeFormat().format(now);
            				Risposta nuovaRisposta = new Risposta(id, testo, autore, dataCorrente, idDomanda);
            				Progetting.invocazione.inserimentoRisposta(nuovaRisposta, new AsyncCallback<Void>(){
            
            					@Override
            					public void onFailure(Throwable caught) {
            						Window.alert("Errore Durante l'iserimento della Risposta !" + caught.getMessage() );
            						
            					}
            
            					@Override
            					public void onSuccess(Void result) {
            						// TODO Auto-generated method stub
            						Window.alert("Risposta inserita con successo !");
            						textArea.setText("");
            						dialogPanel.clear();
            						dialogBox.hide();
            						popupGiudici.hide();
            					}
            				}); // fine inserimentoRisposta
            			}
            		}
            					
            	});// fine creazioneIdRisposta
            }
         });
	}
	
	// Metodo per la creazione "grafica" delle Domande
	
	public static void creaDomanda(VerticalPanel pannelloDomanda, ArrayList<Domanda> result, int i){
		pannelloDomanda = new VerticalPanel();
		final Fieldset campoDomanda = new Fieldset();
		campoDomanda.setStyleName("campodomanda");
		
		final Label labelDomanda = new Label();
		final Label labelData = new Label();
		final Label labelCategoria = new Label();
		final Button pulsanteE = new Button(" Elimina Domanda");
		
		pulsanteE.getElement().setId("buttoneliminadom");
		pulsanteE.setIcon(IconType.TRASH);
		labelDomanda.setStyleName("labeldomanda");
		labelData.setStyleName("labeldata");
		
		String x = "";
		String y = "";
		String z = "";
		
		final String autore = result.get(i).getAutore();
		x = result.get(i).getText();
		y = " > " + result.get(i).getData();
		z = result.get(i).getRoot();
		final Integer idDom = result.get(i).getId();
		
	    labelDomanda.setText(x);
	    labelData.setText(y);
	    
	    
	    campoDomanda.add(labelData);
	    campoDomanda.add(pulsanteE);
	    campoDomanda.add(labelDomanda);
	   
	    
	    if(Login.loginUser.getUsername().equalsIgnoreCase("admin"))
	    {
			pulsanteE.setVisible(true);
		}else{
			pulsanteE.setVisible(false);
		}
	    
	    
	    
	    labelCategoria.setText(z);
		
		pannelloDomanda.add(campoDomanda);
		
		pulsanteE.addClickHandler(new ClickHandler(){

			@Override
			public void onClick(ClickEvent event) {
				// TODO Auto-generated method stub
				idDomanda = idDom;
				Progetting.invocazione.eliminaDomanda(idDomanda, new AsyncCallback<Void>(){

					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						Window.alert("Errore nell'eliminazione della domanda "+ caught.getMessage());
					}

					@Override
					public void onSuccess(Void result) {
						// TODO Auto-generated method stub
						Window.alert("Domanda eliminata correttamente!");
						Home.aggiornamento();
					}
					
				});
			}
			
		});
		
		Progetting.pannelloCentrale.add(pannelloDomanda);
		labelDomanda.addClickHandler(new ClickHandler(){
			public void onClick(ClickEvent event){
				
				idDomanda = idDom;
				visualizzaRisposte();
				dialog();
				testoDomanda.setText(labelDomanda.getText());
				autoreDomanda.setText("autore: " + autore);
				
				catDomanda.setText(labelCategoria.getText());
				dataDomanda.setText(labelData.getText());
				
				dialogBox.setWidget(dialogPanel);
				dialogBox.setPopupPosition(220, 25);
	            dialogBox.show();
	            
				//Window.alert("click sulla label");
				//String txt = labelDomanda.getText();
				//Window.alert(txt);
			}
		});
	} // fine creaDomanda
	
	public static void creaRisposta(ArrayList<Risposta> result, int i){
		if(idDomanda == result.get(i).getIdDomanda()){
			
			final Fieldset campoRisposta = new Fieldset();
			
			
			campoRisposta.setStyleName("camporisposta");
			final Label rispAutore = new Label();
			final Label rispTesto = new Label();
			final Label rispData = new Label();
			final ToggleButton mostraGiudici = new ToggleButton("Mostra Giudici");
			
			final Button buttonVoto = new Button(" Vota");
			
			popupGiudici = new DecoratedPopupPanel();
			mostraGiudici.setStyleName("mostragiudici");
			
			buttonVoto.getElement().setId("buttonvoto");
			buttonVoto.setIcon(IconType.STAR_EMPTY);
			
			rispTesto.setStyleName("testorisposta");
			rispData.setStyleName("datarisposta");
			rispAutore.setStyleName("autorerisposta");
			String rispostaDomanda = result.get(i).getText();
			String autoreDomanda = result.get(i).getAutore();
			String dataRisposta = result.get(i).getData();
			final Integer idRisp = result.get(i).getId();
			final ArrayList<Label> listaJud = cercaGiudici(idRisp);
			final Label lista = new Label();
			final Label labelmedia = new Label();
			
			if(result.get(i).getMediaVoti() == -1){
				labelmedia.setText("NO voti");
			}else{
				float ris = (float) (Math.round(result.get(i).getMediaVoti()*10.0)/10.0);
				labelmedia.setText("MEDIA: " + ris);
			}
			
			labelmedia.setStyleName("labelmedia");
			
			popupGiudici.setStyleName("popupgiudici");
			popupGiudici.setAutoHideEnabled(false);
			
			rispData.setText(" >" + dataRisposta);
			rispAutore.setText(autoreDomanda);
			rispTesto.setText("'" + rispostaDomanda+ "'");
			campoRisposta.add(rispAutore);
			campoRisposta.add(rispData);
			campoRisposta.add(rispTesto);
			campoRisposta.add(labelmedia);
			HTML line = new HTML("<hr/>");
			line.getElement().setId("line");
			campoRisposta.add(line);
			campoRisposta.add(mostraGiudici);
			
			mostraGiudici.addClickHandler(new ClickHandler(){

				@Override
				public void onClick(ClickEvent event) {
					// TODO Auto-generated method stub
					if(mostraGiudici.isDown() == true){
						campoRisposta.add(popupGiudici);
						for(int w = 0; w < listaJud.size(); w++){
							lista.setText(lista.getText() + " " + listaJud.get(w).getText());
						}
						popupGiudici.setWidget(lista);
						popupGiudici.setAnimationEnabled(true);
						Widget source = (Widget) event.getSource();
			            int left = source.getAbsoluteLeft() + 120;
			            int top = source.getAbsoluteTop() - 40;
			            popupGiudici.setPopupPosition(left, top);
						popupGiudici.show();
						
					}
					else {
						lista.setText("");
						popupGiudici.hide();
						
					}
					
				}
				
			});
			
		if(Login.loginUser.getIsGiudice()){
				final Button eliminaRisposta = new Button(" Cancella");
				eliminaRisposta.setIcon(IconType.TRASH);
				eliminaRisposta.getElement().setId("buttoncancrisp");
				Progetting.invocazione.seGiudicata(Login.loginUser.getUsername(), idRisp, new AsyncCallback<Boolean>(){

					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						Window.alert("Errore nel ritornare se risposta già giudicata o no " + caught.getMessage());
					}

					@Override
					public void onSuccess(Boolean result) {
						// TODO Auto-generated method stub
						if(!result && !Login.loginUser.getUsername().equalsIgnoreCase("admin")){
							final ListBox giudizioRisposta = new ListBox();
							giudizioRisposta.setStyleName("giudiziorisposta");
							giudizioRisposta.setSize("15%", "15%");
							giudizioRisposta.addItem("N.V.");
							giudizioRisposta.addItem("0");
							giudizioRisposta.addItem("1");
							giudizioRisposta.addItem("2");
							giudizioRisposta.addItem("3");
							giudizioRisposta.addItem("4");
							giudizioRisposta.addItem("5");
							votoSelezionato = "N.V.";
							campoRisposta.add(giudizioRisposta);
							campoRisposta.add(buttonVoto);
							giudizioRisposta.addChangeHandler(new ChangeHandler(){

								@Override
								public void onChange(ChangeEvent event) {
									// TODO Auto-generated method stub
									votoSelezionato = selectedItem(giudizioRisposta, giudizioRisposta.getSelectedItemText());
								}
								
							});
							buttonVoto.addClickHandler(new ClickHandler(){

								@Override
								public void onClick(ClickEvent event) {
									// TODO Auto-generated method stub
									idRisposta = idRisp;
									//Window.alert("hai selez: " + votoSelezionato);
									Progetting.invocazione.creazioneIdGiudizio(new AsyncCallback<Integer>(){

										@Override
										public void onFailure(Throwable caught) {
											// TODO Auto-generated method stub
											Window.alert("Errore nella creazione Id Giudizio " + caught.getMessage());
										}

										@Override
										public void onSuccess(Integer result) {
											// TODO Auto-generated method stub
											if(votoSelezionato.equalsIgnoreCase("N.V.")){
												Window.alert("Non puoi inserire N.V.!");
											} 
											else {
												int id = result;
												int voto = Integer.parseInt(votoSelezionato);
												String giudice = Login.loginUser.getUsername();
												//Window.alert("id risposta giudicata: " + idRisposta);
												Giudizio nuovoGiudizio = new Giudizio(id, voto, giudice, idRisposta);
												Progetting.invocazione.inserimentoGiudizio(nuovoGiudizio, new AsyncCallback<Void>(){

													@Override
													public void onFailure(Throwable caught) {
														// TODO Auto-generated method stub
														
													}

													@Override
													public void onSuccess(Void result) {
														// TODO Auto-generated method stub
														Window.alert("Giudizio inserito correttamente! ");
														dialogPanel.clear();
					            						dialogBox.hide();
													}
													
												}); // fine inserimento Giudizio
												
											}
										}
										
									}); // fine creazione id giudizio
									
								}
								
							}); // fine buttonVoto click
						} // fine if, costrutto per giudice che non aveva ancora votato questa risposta
						else {
							final Label labelGiudicata = new Label();
							if(Login.loginUser.getUsername().equalsIgnoreCase("admin"))
							{
							  labelGiudicata.setText("");	
							}
							else
							{
							  labelGiudicata.setText(Login.loginUser.getUsername() + " hai già giudicato questa Risposta");
							}
							campoRisposta.add(labelGiudicata);
						}
						
					}
					
				}); // fine controllo se risposta non giudicata in precedenza da quel giudice
				
				campoRisposta.add(eliminaRisposta); 
				eliminaRisposta.addClickHandler(new ClickHandler(){

					@Override
					public void onClick(ClickEvent event) {
						// TODO Auto-generated method stub
						
                        Progetting.invocazione.eliminaRisposta(idRisp, new AsyncCallback<Void>(){

							@Override
							public void onFailure(Throwable caught) {
								// TODO Auto-generated method stub
								Window.alert("Errore nell'eliminazione Risposta" + caught.getMessage());
							}

							@Override
							public void onSuccess(Void result) {
								// TODO Auto-generated method stub
								Window.alert("Risposta Eliminata ! ");
								dialogPanel.clear();	
								dialogBox.hide();
								visualizzaRisposte();
								dialog();
								dialogBox.show();
								
							}});
					}});
				
			} // fine if isGiudice
			else {
				final Label nogiudice = new Label("non sei giudice");
				nogiudice.getElement().setId("nogiudice");
				campoRisposta.add(nogiudice);
			}
			
			dialogPanel.add(campoRisposta);
			
			
		}
	}
	
	public static ArrayList<Label> cercaGiudici(int idRisposta){
		final ArrayList<Label> labels = new ArrayList<Label>();
		Progetting.invocazione.cercaGiudici(idRisposta, new AsyncCallback<ArrayList<String>>(){

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				Window.alert("Errore nel ritorno della lista giudici " + caught.getMessage());
			}

			@Override
			public void onSuccess(ArrayList<String> result) {
				// TODO Auto-generated method stub
				
				for(int i = 0; i < result.size(); i++){
					labels.add(new Label(result.get(i)));
				}
				
			}
		});
		return labels;
	}
	
	private static String selectedItem(ListBox giudizioRisposta, String selezionato){
		String voto = "";
		int itemCount = giudizioRisposta.getItemCount();
	    for (int i = 0; i <= itemCount; i++) {
	        String text = giudizioRisposta.getItemText(i);
	        if (text.equals(selezionato)) {
	            voto = selezionato;
	            return voto;
	        }
	    }
		return voto;
	}
	
	// metodo per visualizzazione delle domande inserite
	public static void visualizzaDomanda(){
		Progetting.invocazione.visualizzaDomanda(new AsyncCallback<ArrayList<Domanda>>(){

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				Window.alert("Errore nel ritornare le domande !" + caught.getMessage());
			}

			@Override
			public void onSuccess(ArrayList<Domanda> result) {
				// TODO Auto-generated method stub
				if(result.isEmpty())
				{
					Progetting.pannelloCentrale.clear();
					Label vuoto = new Label();
					vuoto.getElement().setId("labelvuoto");
					vuoto.setText("Al momento non sono presenti domande all'interno del Sito ! ");
					Progetting.pannelloCentrale.add(vuoto);
				}
				else{
				final String rootDomanda = Albero.labelMessageAlbero.getText();
				Progetting.pannelloCentrale.clear(); //pulizia per evitare la ripetizione
				VerticalPanel pannelloDomanda = new VerticalPanel();
				for(int i=result.size() - 1; i >= 0; i-- ){ // Array decrementale per l'ordinamento cronologico !
                    if (result.get(i).getRoot().contains(rootDomanda)){
                    	
                    	VisualizzazioneDomanda.creaDomanda(pannelloDomanda, result, i);
//                    	
                    }else if (rootDomanda.equalsIgnoreCase("categoria")){
                    	VisualizzazioneDomanda.creaDomanda(pannelloDomanda, result, i);
//                    	
                    }
				}
				}
			}});
	} //fine metodo visualizzaDomanda()
	
	public static void visualizzaRisposte() {
		
		Progetting.invocazione.visualizzaRisposta(new AsyncCallback<ArrayList<Risposta>>(){

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				Window.alert("Errore nel ritornare le risposte "+ caught.getMessage());
			}

			@Override
			public void onSuccess(ArrayList<Risposta> result) {
				// TODO Auto-generated method stub
				final ArrayList<Risposta> appoggio = new ArrayList<Risposta>(); //media
				final ArrayList<Risposta> appoggio2 = new ArrayList<Risposta>(); //data
				
				for(int i=result.size() - 1; i >= 0; i--)
				{
						if(result.get(i).getMediaVoti() != -1 ) 
						{
							appoggio.add(result.get(i));
						}else{
							appoggio2.add(result.get(i));
						}
					
				}
					
					Collections.sort(appoggio, new Comparator<Risposta>() {
						

						@Override
						public int compare(Risposta o1, Risposta o2) {
							// TODO Auto-generated method stub
							if (o1.getMediaVoti().compareTo(o2.getMediaVoti()) == 0){
							return o1.getData().compareTo(o2.getData());	
							}
							else{
							return o1.getMediaVoti().compareTo(o2.getMediaVoti());
							}
						}
						
						});
				
				for(int j= appoggio.size() -1 ; j >= 0; j--)
				{
					creaRisposta(appoggio, j);
					
				}
				
				for(int j = 0; j < appoggio2.size(); j++)
				{
					creaRisposta(appoggio2, j);
					
				}
				
				if (!Login.loginUser.getUsername().isEmpty() && !Login.loginUser.getUsername().equalsIgnoreCase("admin"))
				{
				 dialogPanel.add(textArea);
				 dialogPanel.add(inserisciRisposta);
				 
				}
				dialogPanel.add(annulla);
			}
	
		});
		
	}

}
