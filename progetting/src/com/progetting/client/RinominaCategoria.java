package com.progetting.client;

import com.github.gwtbootstrap.client.ui.Fieldset;
import com.github.gwtbootstrap.client.ui.Label;

import com.github.gwtbootstrap.client.ui.TextBox;
import com.github.gwtbootstrap.client.ui.constants.IconType;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.regexp.shared.RegExp;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.github.gwtbootstrap.client.ui.Button;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TreeItem;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.progetting.shared.Utente;

public class RinominaCategoria {
	
	static Button rinCategoria = new Button("Rinomina Categoria");
	static String vecchio;
	static Label title = new Label("Scegli la categoria da rinominare");
	
	static void rinominaCategoria()
	{
		rinCategoria.setStyleName("btn btn-primary btn-large rinominaButton");
		rinCategoria.setIcon(IconType.EDIT_SIGN);
		rinCategoria.addClickHandler(new ClickHandler(){

			@Override
			public void onClick(ClickEvent event) {
				
				//pulsante visibilità
				RootPanel.get("destra").clear();
				RootPanel.get("contenuto").clear();
				InserimentoCategoria.insCategoria.setEnabled(true);
				NominaGiudice.nominaGiudice.setEnabled(true);
				rinCategoria.setEnabled(false);
				final VerticalPanel rinCat = new VerticalPanel();
				final Label testoSelez = new Label();
				final Label avviso = new Label("Rinomina in : ");
				final TextBox testoRinominato = new TextBox();
				final Button inviaRin = new Button("Rinomina la Categoria");
				title.getElement().setId("titlerin");
				inviaRin.setStyleName("bottoniInvia");
				inviaRin.setIcon(IconType.OK_CIRCLE);
				final Fieldset campo = new Fieldset();
				Albero.visualizzaAlbero();
				rinCat.add(title);
				rinCat.add(Albero.tree);
				rinCat.add(Albero.labelMessageAlbero);
				rinCat.add(testoSelez);
				
				Albero.labelMessageAlbero.setText("");
				
				Albero.tree.addSelectionHandler(new SelectionHandler<TreeItem>() 
		    	{
			         public void onSelection(SelectionEvent<TreeItem> event) 
			         {
			        	 if(event.getSelectedItem().getParentItem().getText()!="categoria"){
			        		 vecchio = event.getSelectedItem().getParentItem().getText() + "/" + event.getSelectedItem().getText();
			        	 }
			        	 else{
			        		 vecchio = event.getSelectedItem().getText();
			        	 }
			        	 
			        	 testoRinominato.setText(null);
			        	 
			        	 if(event.getSelectedItem().getText().equalsIgnoreCase("categoria")){
			        		 rinCat.remove(campo);
			        		 inviaRin.setVisible(false);
			        		 testoSelez.setText("Non è possibile rinominare la root : Categoria ! Selezionare una Category o Sub-Category");
			         }else{
			        	    inviaRin.setVisible(true);
			        	    campo.add(avviso);
			        	    campo.add(testoRinominato);
			            	rinCat.add(campo);
			            	Albero.trovaPadre(event);
			            	testoSelez.setText("Rinomina la Category o Sub-Category: " + event.getSelectedItem().getText());
			        	   }
			        	 
			        	 rinCat.add(inviaRin);
			        }
			     });
				
				
				
				inviaRin.addClickHandler(new ClickHandler(){

					@Override
					public void onClick(ClickEvent event) {
						// TODO Auto-generated method stub
						RegExp p = RegExp.compile("[^a-z0-9-^A-Z ]");
						final boolean controlloContenuto = p.test(testoRinominato.getText());
						if(controlloContenuto || testoRinominato.getText().isEmpty())
						{
							//stringa errata
							Window.alert("Il nome della categoria deve essere alfanumerico! Non inserire caratteri speciali.");
							testoRinominato.setText(null);
						}else{
							//stringa corretta
							
							String nuovo = testoRinominato.getText();
							Window.alert("nuovo: " + nuovo + " vecchio: " + vecchio);
							Progetting.invocazione.rinominaCategoria(vecchio, nuovo, new AsyncCallback<Void>(){

								@Override
								public void onFailure(Throwable caught) {
									Window.alert("Errore nel rinomina Categoria" + caught.getMessage());
									
								}

								@Override
								public void onSuccess(Void result) {
									Window.alert("Categoria Rinominata con Successo !");
									Home.aggiornamento();
									
								}} );
							
							
							
						}
						
					}});
				
				RootPanel.get("destra").clear();
				RootPanel.get("contenuto").add(rinCat);
				
			}});
	}

}
